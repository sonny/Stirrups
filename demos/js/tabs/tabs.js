// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Tabs
 *
 * GtkTextView can position text at fixed positions, using tabs.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GLib, Gtk, Pango } = imports.gi;

Pango.TabArray.newWithPositions = function(positionsInPixels = false, alignments = [], positions = []) {
    if (positions.length < alignments.length)
        throw new Error("Not enough positions");

    let tabArray = Pango.TabArray.new(alignments.length, positionsInPixels);

    alignments.forEach((alignment, index) => {
        tabArray.set_tab(index, alignment, positions[index]);
    });

    // Free the tab array once it is consumed (i.e. copied by the text view).
    GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => tabArray.free());

    return tabArray;
};

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application, title: "Tabs",
        resizable: false,
        defaultWidth: 330, defaultHeight: 330,
        child: new Gtk.ScrolledWindow({
            child: new Gtk.TextView({
                wrapMode: Gtk.WrapMode.WORD,
                topMargin: 20, bottomMargin: 20,
                leftMargin: 20, rightMargin: 20,
                buffer: new Gtk.TextBuffer({
                    text: "one\ttwo\tthree\nfour\tfive\tsix\nseven\teight\tnine",
                }),
                tabs: Pango.TabArray.newWithPositions(
                    true,
                    new Array(3).fill(Pango.TabAlign.LEFT),
                    [0, 100, 200]
                ),
            }),
        }),
    }).present();
});

application.run([]);
