// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Transition
 *
 * This demo shows a simple example of a custom layout manager
 * and a widget using it. The layout manager places the children
 * of the widget in a grid or a circle.
 *
 * The widget is animating the transition between the two layouts.
 *
 * Click to start the transition.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, GLib, GObject, Graphene, Gtk } = imports.gi;
const System = imports.system;
void imports.demolayout;

// XXX: Workaround for the "offending callback" GJS critical error.
Gtk.Widget.prototype.addTickCallbackOld = Gtk.Widget.prototype.add_tick_callback;
Gtk.Widget.prototype.add_tick_callback = function(callback) {
    System.gc();
    let time = Date.now();

    return this.addTickCallbackOld(function(...args) {
        // Anticipate the next garbage collection to prevent tick
        // callbacks from "appearing" during a garbage collection.
        if ((Date.now() - time) >= 6900) {
            System.gc();
            time = Date.now();
        }

        return callback(...args);
    });
};

// This is a trivial child widget just for demo purposes.
// It draws a 32x32 square in fixed color.
const DemoChild = GObject.registerClass(class extends Gtk.Widget {
    _init(params) {
        super._init(params);

        this._rgba = new Gdk.RGBA();
        this._rgba.parse(this.tooltipText);
    }

    vfunc_measure(orientation_, forSize_) {
        return [32, 32, -1, -1];
    }

    vfunc_snapshot(snapshot) {
        snapshot.append_color(
            this._rgba,
            new Graphene.Rect().init(0, 0, this.get_width(), this.get_height())
        );
    }
});

// Transition duration in ms.
// We take half a second to go from one layout to the other.
const DURATION = 500;

// The widget is controlling the transition by setting the progress
// property of the demo layout in a tick callback.
const DemoWidget = GObject.registerClass(class extends Gtk.Widget {
    _init(params) {
        super._init(params);

        // Whether we go 0 -> 1 or 1 -> 0.
        this._backward = 0;

        let gesture = new Gtk.GestureClick();
        gesture.connect('pressed', () => {
            if (this._tickId)
                return;

            this._startTime = Date.now();
            this._tickId = this.add_tick_callback(() => {
                this.queue_allocate();
                let elapsed = Date.now() - this._startTime;

                this.layoutManager.progress = this._backward ?
                    1 - elapsed / DURATION : elapsed / DURATION;

                if (elapsed >= DURATION) {
                    this._backward = !this._backward;

                    this.layoutManager.progress = this._backward ? 1 : 0;
                    // Keep things interesting by shuffling the positions.
                    if (!this._backward)
                        this.layoutManager.shuffle(this);

                    this._tickId = 0;
                    return GLib.SOURCE_REMOVE;
                }

                return GLib.SOURCE_CONTINUE;
            });
        });
        this.add_controller(gesture);
    }

    vfunc_unrealize() {
        super.vfunc_unrealize();

        [...this].forEach(child => child.unparent());
    }

    addChild(child) {
        child.set_parent(this);
    }
});

// Here is where we use our custom layout manager.
DemoWidget.set_layout_manager_type(GObject.type_from_name('DemoLayout'));

const colors = [
    'red', 'orange', 'yellow', 'green',
    'blue', 'grey', 'magenta', 'lime',
    'yellow', 'firebrick', 'aqua', 'purple',
    'tomato', 'pink', 'thistle', 'maroon',
];

let application = new Gtk.Application();

application.connect('activate', () => {
    let widget = new DemoWidget();

    colors.forEach(color => widget.addChild(new DemoChild({
        tooltipText: color,
        marginStart: 4, marginEnd: 4,
        marginTop: 4, marginBottom: 4,
    })));

    new Gtk.Window({
        application, title: "Layout Manager — Transition",
        defaultWidth: 600, defaultHeight: 600,
        child: widget,
    }).present();
});

application.run([]);
