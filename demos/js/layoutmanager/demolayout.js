// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

const { Gdk, GLib, GObject, Gtk } = imports.gi;
const System = imports.system;

// 2 JS objects may be different although the underlying GObject objects
// are equal.
const equalSymbol = Symbol('gobject equality test');
Gtk.Widget.prototype[equalSymbol] = function(object) {
    return System.addressOfGObject(this) == System.addressOfGObject(object);
};

// On the contrary to the original GTK demo, the child position is stored
// in a LayoutChild object. Indeed, the layout manager, as a JS object, is
// not permanent. It is created and disposed many times. All JS properties
// attached to it would be lost.
const DemoLayoutChild = GObject.registerClass({
    Properties: {
        'position': GObject.ParamSpec.uint(
            'position', "Position", "The position of the child in the layout",
            GObject.ParamFlags.READWRITE, 0, GLib.MAXUINT32, 0
        )
    },
}, class extends Gtk.LayoutChild {});

GObject.registerClass({
    GTypeName: 'DemoLayout',
    Properties: {
        'progress': GObject.ParamSpec.float(
            'progress', "Progress", "The transition progress",
            GObject.ParamFlags.READWRITE, 0, 1, 0
        )
    },
}, class extends Gtk.LayoutManager {
    vfunc_get_request_mode() {
        return Gtk.SizeRequestMode.CONSTANT_SIZE;
    }

    vfunc_measure(widget, orientation, forSize_) {
        let [minimumSize, naturalSize] = [0, 0];

        [...widget].forEach(child => {
            if (!child.should_layout())
                return;

            let [childMin, childNat] = child.measure(orientation, -1);
            minimumSize = Math.max(minimumSize, childMin);
            naturalSize = Math.max(naturalSize, childNat);
        });

        // A back-of-a-napkin calculation to reserve enough
        // space for arranging 16 children in a circle.
        let minimum = 16 * minimumSize / Math.PI + minimumSize;
        let natural = 16 * naturalSize / Math.PI + naturalSize;
        return [minimum, natural, -1, -1];
    }

    vfunc_allocate(widget, width, height, baseline_) {
        let [childWidth, childHeight] = [0, 0];
        let children = [...widget];

        children.forEach(child => {
            if (!child.should_layout())
                return;

            let childMinReq = child.get_preferred_size()[0];
            childWidth = Math.max(childWidth, childMinReq.width);
            childHeight = Math.max(childHeight, childMinReq.height);
        });

        // The center of our layout.
        let [x0, y0] = [width / 2, height / 2];

        // The radius for our circle of children.
        let r = 8 * childWidth / Math.PI;

        children.forEach((child, index) => {
            if (!child.should_layout())
                return;

            let a = this.get_layout_child(child).position * Math.PI / 8;
            let childMinReq = child.get_preferred_size()[0];

            // The grid position of child.
            let gx = x0 + (index % 4 - 2) * childWidth;
            let gy = y0 + (parseInt(index / 4) - 2) * childHeight;

            // The circle position of child. Note that we are adjusting the
            // position by half the child size to place the center of child
            // on a centered circle. This assumes that the children don't use
            // align flags or uneven margins that would shift the center.
            let cx = x0 + Math.sin(a) * r - childMinReq.width / 2;
            let cy = y0 + Math.cos(a) * r - childMinReq.height / 2;

            // We interpolate between the two layouts according to the progress
            // value that has been set on the layout.
            let x = this.progress * cx + (1 - this.progress) * gx;
            let y = this.progress * cy + (1 - this.progress) * gy;

            child.size_allocate(
                new Gdk.Rectangle({ x, y, width: childWidth, height: childHeight }),
                -1
            );
        });
    }

    // The layout child is created the first time this.get_layout_child(forChild) is called.
    vfunc_create_layout_child(widget, forChild) {
        return new DemoLayoutChild({
            layoutManager: this,
            childWidget: forChild,
            position: [...widget].findIndex(child => child[equalSymbol](forChild)),
        });
    }

    // Shuffle the circle positions of the children.
    // Should be called when we are in the grid layout.
    shuffle(widget) {
        let children = [...widget];

        for (let i = 0; i < children.length; i++) {
            let j = GLib.random_int_range(0, i + 1);
            let layoutChild1 = this.get_layout_child(children[i]);
            let layoutChild2 = this.get_layout_child(children[j]);

            [layoutChild1.position, layoutChild2.position] = [layoutChild2.position, layoutChild1.position];
        }
    }
});
