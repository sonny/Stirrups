// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Type to Search
 *
 * GtkSearchEntry provides an entry that is ready for search.
 *
 * Search entries have their "search-changed" signal delayed and
 * should be used when the search operation is slow, such as big
 * datasets to search, or online searches.
 *
 * GtkSearchBar allows have a hidden search entry that 'springs
 * into action' upon keyboard input.
 *
 * Difference with GTK4 Demos: remove a useless middle box.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GObject, Gtk } = imports.gi;

let application = new Gtk.Application();

application.connect('activate', () => {
    // ⛓️ Connect a search entry, a search bar, a toggle button and a result label.
    let searchBar = new Gtk.SearchBar({
        showCloseButton: false,
        child: new Gtk.SearchEntry({ halign: Gtk.Align.CENTER }),
    });
    searchBar.connect_entry(searchBar.child);

    let searchButton = new Gtk.ToggleButton({ iconName: 'system-search-symbolic' });
    searchButton.bind_property('active', searchBar, 'search-mode-enabled', GObject.BindingFlags.BIDIRECTIONAL);

    let resultLabel = new Gtk.Label();
    searchBar.child.connect('search-changed', searchEntry => resultLabel.set_text(searchEntry.text));

    // 📍 Pack them in a window.
    let window = new Gtk.Window({
        application, title: "Type to Search",
        widthRequest: 200, resizable: false,
        child: Gtk.Box.new(Gtk.Orientation.VERTICAL, 0),
    });

    window.set_titlebar(new Gtk.HeaderBar());
    window.get_titlebar().pack_end(searchButton);

    let hbox = new Gtk.Box({
        marginTop: 18, marginBottom: 18,
        marginStart: 18, marginEnd: 18,
        spacing: 10,
    });
    hbox.append(new Gtk.Label({ label: "Searching for:", xalign: 0 }));
    hbox.append(resultLabel);

    window.child.append(searchBar);
    window.child.append(hbox);

    // ❗️ Hook the search bar to key presses.
    searchBar.set_key_capture_widget(window);

    window.present();
});

application.run([]);
