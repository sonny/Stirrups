// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Password Entry
 *
 * GtkPasswordEntry provides common functionality of
 * entries that are used to enter passwords and other
 * secrets.
 *
 * It will display a warning if CapsLock is on, and it
 * can optionally provide a way to see the text.
 */

imports.gi.versions['Gtk'] = '4.0';
const Gtk = imports.gi.Gtk;

let application = new Gtk.Application();

application.connect('activate', () => {
    let entry1 = new Gtk.PasswordEntry({
        showPeekIcon: true,
        placeholderText: "Password",
        activatesDefault: true,
    });

    let entry2 = new Gtk.PasswordEntry({
        showPeekIcon: true,
        placeholderText: "Confirm",
        activatesDefault: true,
    });

    let button = new Gtk.Button({
        label: "_Done",
        useUnderline: true,
        cssClasses: ['suggested-action', 'text-button'],
        sensitive: false,
    });

    button.update = function() {
        this.sensitive = entry1.text.length && entry1.text == entry2.text;
    };
    button.connect('clicked', button => button.root.destroy());
    entry1.connect('notify::text', () => button.update());
    entry2.connect('notify::text', () => button.update());

    let window = new Gtk.Window({
        application, title: "Choose a Password",
        // The window will be closed with the "Done" button.
        deletable: false,
        resizable: false,
        child: new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginTop: 18, marginBottom: 18,
            marginStart: 18, marginEnd: 18,
            spacing: 6,
        }),
        // Entries have the 'activates-default' property set.
        defaultWidget: button,
    });

    window.set_titlebar(new Gtk.HeaderBar());
    window.get_titlebar().pack_end(button);

    window.child.append(entry1);
    window.child.append(entry2);

    window.present();
});

application.run([]);
