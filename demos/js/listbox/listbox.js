// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Complex
 *
 * GtkListBox allows lists with complicated layouts, using
 * regular widgets supporting sorting and filtering.
 */


/* TODO: Report improvements:
 *  '@GTKtoolkit' -> 'GTKtoolkit' in message parsing (or the inverse operation in messages.txt).
 *  Use textures instead of pixbufs ?
 *  "Hide" and "Expand" localization (or make ui file labels untranslatable).
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, GLib, GObject, Gtk } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');
const _ = imports.gettext.domain('gtk40').gettext;

// TODO: Report feature request to GJS.
// Add the 'swapped' flag support to the template's builder scope.
// The function is only meant to be generic because the handlers do not
// use the arguments in this demo. Of course the easiest solution would
// be to remove the 'swapped' flags from the UI definition.
imports.overrides.Gtk._createClosure = function(builder, thisArg, handlerName, swapped, connectObject) {
    if (typeof thisArg[handlerName] === 'undefined') {
        throw new Error(`A handler called ${handlerName} was not ` +
            `defined on ${thisArg}`);
    }

    if (swapped) {
        return connectObject ?
            (...args) => thisArg[handlerName](connectObject, ...args.slice(1), args[0]) :
            (...args) => thisArg[handlerName](...args.slice(1), args[0]);
    } else {
        return connectObject ?
            (...args) => thisArg[handlerName](...args, connectObject) :
            (...args) => thisArg[handlerName](...args);
    }
};

const avatarTextureOther = Gdk.Texture.new_from_file(directory.get_child('apple-red.png'));

const parseLine = function(line) {
    let [
        id, senderName, senderNick, message, time, replyTo, resentBy,
        nFavorites, nReshares,
    ] = line.split('|');

    return {
        id, senderName, senderNick, message, time, replyTo, resentBy,
        nFavorites: parseInt(nFavorites), nReshares: parseInt(nReshares),
    };
};

const MessageRow = GObject.registerClass({
    GTypeName: 'GtkMessageRow',
    Template: directory.get_child('listbox.ui').get_uri(),
    InternalChildren: [
        'avatar_image', 'content_label', 'detailed_time_label', 'details_revealer',
        'expand_button', 'extra_buttons_box', 'n_favorites_label', 'n_reshares_label',
        'resent_box', 'resent_by_button', 'short_time_label', 'source_name', 'source_nick',
    ],
    Properties: {
        'message': GObject.ParamSpec.jsobject(
            'message', "Message", "The message this displays",
            GObject.ParamFlags.READWRITE || GObject.ParamFlags.CONSTRUCT_ONLY
        ),
    },
}, class extends Gtk.ListBoxRow {
    _init(params) {
        super._init(params);

        this._update();
    }

    _update() {
        this._source_name.label = this.message.senderName;
        this._source_nick.label = this.message.senderNick;
        this._content_label.label = this.message.message;
        let dateTime = GLib.DateTime.new_from_unix_utc(this.message.time);
        this._short_time_label.label = dateTime.format('%e %b %y');
        this._detailed_time_label.label = dateTime.format('%X - %e %b %Y');

        this._n_favorites_label.label = `<b>${this.message.nFavorites}</b>\nFavorites`;
        this._n_favorites_label.visible = !!this.message.nFavorites;
        this._n_reshares_label.label = `<b>${this.message.nReshares}</b>\nReshares`;
        this._n_reshares_label.visible = !!this.message.nReshares;

        this._resent_by_button.label = this.message.resentBy || "";
        this._resent_box.visible = !!this.message.resentBy;

        if (this.message.senderNick == 'GTKtoolkit') {
            this._avatar_image.iconName = 'org.gtk.Demo4';
            this._avatar_image.iconSize = Gtk.IconSize.LARGE;
        } else {
            this._avatar_image.paintable = avatarTextureOther;
        }
    }

    // Builder handler.
    expand_clicked() {
        this._details_revealer.revealChild = !this._details_revealer.revealChild;
        this._expand_button.label = this._details_revealer.revealChild ? _("Hide") : _("Expand");
    }

    // Builder handler.
    favorite_clicked() {
        this.message.nFavorites++;
        this._update();
    }

    // Builder handler.
    reshare_clicked() {
        this.message.nReshares++;
        this._update();
    }

    vfunc_state_flags_changed(...args) {
        this._extra_buttons_box.visible =
            !!(this.get_state_flags() & (Gtk.StateFlags.PRELIGHT | Gtk.StateFlags.SELECTED));

        super.vfunc_state_flags_changed(...args);
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let listBox = new Gtk.ListBox({
        activateOnSingleClick: false,
    });
    listBox.set_sort_func((a, b) => b.message.time - a.message.time);
    listBox.connect('row-activated', (listbox_, row) => row.expand_clicked());

    let contents = directory.get_child('messages.txt').load_contents(null)[1];
    let lines = ByteArray.toString(contents).trim().split('\n');
    lines.forEach(line => {
        let message = parseLine(line);
        listBox.insert(new MessageRow({ message }), -1);
    });

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 12,
    });
    vbox.append(new Gtk.Label({
        label: "Messages from GTK and friends",
    }));
    vbox.append(new Gtk.ScrolledWindow({
        hscrollbarPolicy: Gtk.PolicyType.NEVER,
        vexpand: true,
        child: listBox,
    }));

    new Gtk.Window({
        application,
        title: "List Box — Complex",
        defaultWidth: 400, defaultHeight: 600,
        child: vbox,
    }).present();
});

application.run([]);
