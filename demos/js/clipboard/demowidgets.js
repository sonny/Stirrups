// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* exported DemoEntry, DemoImage */

const { Gdk, Gio, GObject, Gtk } = imports.gi;
const _ = imports.gettext.domain('gtk40').gettext;

var DemoEntry = GObject.registerClass(class DemoEntry extends Gtk.Entry {
    _init(params) {
        let target = new Gtk.DropTarget({ actions: Gdk.DragAction.COPY });
        // The delegate editable has already a drop target for strings.
        target.set_gtypes([Gdk.Paintable.$gtype]);
        target.connect('drop', this._dragDrop.bind(this));

        super._init(params);

        this.get_delegate().add_controller(target);
    }

    _dragDrop(target, valueContents) {
        if (valueContents instanceof Gtk.IconPaintable) {
            this.get_delegate().text = valueContents.get_icon_name();
            return true;
        }

        return false;
    }

    copy() {
        this.get_clipboard().set(this.text);
    }

    pasteAsync() {
        // The clipboard contents are get asynchronously.
        // XXX: Promise.any is not currently available in GJS.
        return Promise.allSettled([
            new Promise((resolve, reject) => {
                this.get_clipboard().read_text_async(null, (clipboard, result) => {
                    try {
                        this.text = clipboard.read_text_finish(result);
                        resolve();
                    } catch(e) {
                        reject(e);
                    }
                });
            }),
            new Promise((resolve, reject) => {
                this.get_clipboard().read_value_async(Gdk.Paintable.$gtype, 0, null, (clipboard, result) => {
                    try {
                        let paintable = clipboard.read_value_finish(result);
                        if (!(paintable instanceof Gtk.IconPaintable))
                            throw new Error();

                        this.text = paintable.get_icon_name();
                        resolve();
                    } catch(e) {
                        reject(e);
                    }
                });
            }),
        ]).then(values => {
            if (values.every(value => value.status == 'rejected'))
                throw values[0].reason;
        });
    }
});

var DemoImage = GObject.registerClass({
    Properties: {
        'icon-name': GObject.ParamSpec.string(
            'icon-name', "Icon name", "The icon name",
            GObject.ParamFlags.READWRITE, null
        ),
    },
}, class DemoImage extends Gtk.Widget {
    _init(params) {
        this._image = new Gtk.Image({ pixelSize: 48 });

        this._popover = new Gtk.PopoverMenu({ menuModel: new Gio.Menu() });
        this._popover.menuModel.append(_("_Copy"), 'clipboard.copy');
        this._popover.menuModel.append(_("_Paste"), 'clipboard.paste');

        let copyAction = new Gio.SimpleAction({ name: 'copy' });
        copyAction.connect('activate', this._copy.bind(this));
        let pasteAction = new Gio.SimpleAction({ name: 'paste' });
        pasteAction.connect('activate', this._paste.bind(this));
        let actionGroup = new Gio.SimpleActionGroup();
        actionGroup.add_action(copyAction);
        actionGroup.add_action(pasteAction);

        let gesture = new Gtk.GestureClick({ button: Gdk.BUTTON_SECONDARY });
        gesture.connect('pressed', () => this._popover.popup());

        let source = new Gtk.DragSource();
        source.connect('prepare', this._dragPrepare.bind(this));
        source.connect('drag-begin', this._dragBegin.bind(this));

        let target = new Gtk.DropTarget({ actions: Gdk.DragAction.COPY });
        target.set_gtypes([Gdk.Paintable.$gtype, GObject.TYPE_STRING]);
        target.connect('drop', this._dragDrop.bind(this));

        super._init(params);

        this._image.set_parent(this);
        this._popover.set_parent(this);
        this.insert_action_group('clipboard', actionGroup);
        this.add_controller(gesture);
        this.add_controller(source);
        this.add_controller(target);
    }

    get _paintable() {
        switch (this._image.storageType) {
            case Gtk.ImageType.ICON_NAME:
                return Gtk.IconTheme.get_for_display(this.get_display())
                    .lookup_icon(this._image.iconName, null, 48, 1, this._image.get_direction(), 0);
            case Gtk.ImageType.PAINTABLE:
                return this._image.paintable;
            default:
                log(`Image storage type ${this._image.storageType} not handled`);
                return null;
        }
    }

    set _paintable(paintable) {
        this._image.paintable = paintable;
        this.notify('icon-name');
    }

    _copy() {
        let value = new GObject.Value();
        value.init(Gdk.Paintable.$gtype);
        value.set_object(this._paintable);
        this.get_clipboard().set(value);
    }

    _paste() {
        let value = new GObject.Value();
        value.init(Gdk.Paintable.$gtype);

        try {
            this.get_clipboard().get_content().get_value(value);
            this._paintable = value.get_object();
        } catch(e) {
            value.unset();
            value.init(GObject.TYPE_STRING);

            try {
                this.get_clipboard().get_content().get_value(value);
                if (Gtk.IconTheme.get_for_display(this.get_display()).has_icon(value.get_string()))
                    this.iconName = value.get_string();
            } catch(e) {}
        }
    }

    _dragPrepare() {
        let value = new GObject.Value();
        value.init(Gdk.Paintable.$gtype);
        value.set_object(this._paintable);

        return Gdk.ContentProvider.new_for_value(value);
    }

    _dragBegin(source, drag) {
        let icon = Gtk.DragIcon.get_for_drag(drag);
        let { iconName, paintable, pixelSize } = this._image;
        icon.child = new Gtk.Image(paintable ? { paintable, pixelSize } : { iconName, pixelSize });
    }

    _dragDrop(target, valueContents) {
        if (typeof valueContents == 'object'
            && valueContents instanceof Gdk.Paintable) {
            this._paintable = valueContents;
            return true;
        } else if (typeof valueContents == 'string'
            && Gtk.IconTheme.get_for_display(this.get_display()).has_icon(valueContents)) {
            this.iconName = valueContents;
            return true;
        }

        return false;
    }

    vfunc_unrealize() {
        super.vfunc_unrealize();

        this._image.unparent();
        this._popover.unparent();
    }

    get iconName() {
        return this._image.iconName;
    }

    set iconName(iconName) {
        if (this.iconName != iconName) {
            this._image.iconName = iconName;
            this.notify('icon-name');
        }
    }
});
DemoImage.set_layout_manager_type(Gtk.BinLayout.$gtype);
