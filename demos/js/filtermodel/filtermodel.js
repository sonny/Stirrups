// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Filter Model
 *
 * This example demonstrates how GtkTreeModelFilter can be used not
 * just to show a subset of the rows, but also to compute columns
 * that are not actually present in the underlying model.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GObject, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

const Column = { WIDTH: 0, HEIGHT: 1, AREA: 2, SQUARE: 3 };

function formatNumber(columnNumber, column, renderer, model, iter) {
    renderer.text = String(model.get_value(iter, columnNumber));
}

function onCellEdited(columnNumber, model, renderer, pathString, newText) {
    model.set(model.get_iter_from_string(pathString)[1], [columnNumber], [Number(newText)]);
}

function modifyFunc(filterModel, iter, columnNumber) {
    let childIter = filterModel.convert_iter_to_child_iter(iter);

    switch(columnNumber) {
        case Column.WIDTH:
        case Column.HEIGHT:
            return filterModel.childModel.get_value(childIter, columnNumber);
        case Column.AREA:
            return filterModel.childModel.get_value(childIter, Column.WIDTH)
                * filterModel.childModel.get_value(childIter, Column.HEIGHT);
        case Column.SQUARE:
            return filterModel.childModel.get_value(childIter, Column.WIDTH)
                == filterModel.childModel.get_value(childIter, Column.HEIGHT);
        default:
            throw new Error(`Unknown column ${columnNumber}`);
    }
}

function visibleFunc(childModel, childIter) {
    return childModel.get_value(childIter, Column.WIDTH) < 10;
}

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('filtermodel.ui').get_path());

    let store = builder.get_object('liststore1');

    builder.get_object('treeviewcolumn1').set_cell_data_func(
        builder.get_object('cellrenderertext1'), formatNumber.bind(null, Column.WIDTH)
    );

    builder.get_object('cellrenderertext1').connect(
        'edited', onCellEdited.bind(null, Column.WIDTH, store)
    );

    builder.get_object('treeviewcolumn2').set_cell_data_func(
        builder.get_object('cellrenderertext2'), formatNumber.bind(null, Column.HEIGHT)
    );

    builder.get_object('cellrenderertext2').connect(
        'edited', onCellEdited.bind(null, Column.HEIGHT, store)
    );

    builder.get_object('treeviewcolumn3').set_cell_data_func(
        builder.get_object('cellrenderertext3'), formatNumber.bind(null, Column.WIDTH)
    );

    builder.get_object('treeviewcolumn4').set_cell_data_func(
        builder.get_object('cellrenderertext4'), formatNumber.bind(null, Column.HEIGHT)
    );

    builder.get_object('treeviewcolumn5').set_cell_data_func(
        builder.get_object('cellrenderertext5'), formatNumber.bind(null, Column.AREA)
    );

    builder.get_object('treeviewcolumn6').add_attribute(
        builder.get_object('cellrendererpixbuf1'), 'visible', Column.SQUARE
    );

    // "Comuted Columns" tree.
    {
        let treeView = builder.get_object('treeview2');
        treeView.model = new Gtk.TreeModelFilter({ childModel: store });
        treeView.model.set_modify_func(
            [GObject.TYPE_INT, GObject.TYPE_INT, GObject.TYPE_INT, GObject.TYPE_BOOLEAN], modifyFunc
        );

        builder.get_object('treeviewcolumn7').set_cell_data_func(
            builder.get_object('cellrenderertext6'), formatNumber.bind(null, Column.WIDTH)
        );

        builder.get_object('treeviewcolumn8').set_cell_data_func(
            builder.get_object('cellrenderertext7'), formatNumber.bind(null, Column.HEIGHT)
        );
    }

    // "Filtered" tree.
    {
        let treeView = builder.get_object('treeview3');
        treeView.model = new Gtk.TreeModelFilter({ childModel: store });
        treeView.model.set_visible_func(visibleFunc);
        treeView.model.refilter();
    }

    let window = builder.get_object('window1');
    window.set_application(application);
    window.present();
});

application.run([]);
