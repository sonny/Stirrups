// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Rotated Text
 *
 * This demo shows how to use PangoCairo to draw rotated and transformed
 * text. The right pane shows a rotated GtkLabel widget.
 *
 * In both cases, a custom PangoCairo shape renderer is installed to draw
 * a red heart using cairo drawing operations instead of the Unicode heart
 * character.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gtk, Pango, PangoCairo } = imports.gi;
const Cairo = imports.cairo;

let application = new Gtk.Application();

application.connect('activate', () => {
    let drawingArea = new Gtk.DrawingArea();
    drawingArea.set_draw_func((drawingArea, cr, width, height) => {
        let layout = drawingArea.create_pango_layout("Pango power!\nPango power!\nPango power!");
        layout.set_font_description(Pango.FontDescription.from_string('sans bold 34'));

        cr.moveTo(30, 20);
        PangoCairo.layout_path(cr, layout);

        let pattern = new Cairo.LinearGradient(0, 0, width, height);
        pattern.addColorStopRGB(0.0, 1, 0, 0);
        pattern.addColorStopRGB(0.2, 1, 0, 0);
        pattern.addColorStopRGB(0.3, 1, 1, 0);
        pattern.addColorStopRGB(0.4, 0, 1, 0);
        pattern.addColorStopRGB(0.6, 0, 1, 1);
        pattern.addColorStopRGB(0.7, 0, 0, 1);
        pattern.addColorStopRGB(0.8, 1, 0, 1);
        pattern.addColorStopRGB(1.0, 1, 0, 1);
        cr.setSource(pattern);
        cr.fillPreserve();

        cr.setSourceRGB(0, 0, 0);
        cr.setLineWidth(0.5);
        cr.stroke();

        cr.$dispose();
    });

    new Gtk.Window({
        application,
        title: "Text Mask",
        widthRequest: 400, heightRequest: 200, resizable: true,
        child: drawingArea,
    }).present();
});

application.run([]);
