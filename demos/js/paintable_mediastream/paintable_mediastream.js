// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Media Stream
 *
 * GdkPaintable is also used by the GtkMediaStream class.
 *
 * This demo code turns the nuclear media_stream into the object
 * GTK uses for videos. This allows treating the icon like a
 * regular video, so we can for example attach controls to it.
 *
 * After all, what good is a media_stream if one cannot pause
 * it.
 *
 * XXX: GtkMediaStream is not correctly implemented because its API
 * is not currently usable from language bindings.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, GLib, GObject, Graphene, Gtk } = imports.gi;

// Do a full rotation in 5 seconds. We do not save steps here but real timestamps.
// GtkMediaStream uses microseconds, so we will do so, too.
const DURATION = 5 * GLib.USEC_PER_SEC;
const RADIUS = 0.3;

const NuclearMediaStream = GObject.registerClass(class NuclearMediaStream extends Gtk.MediaStream {
    _init(params, animate = true, progress = 0) {
        super._init(params);

        // This variable stores the progress of our video.
        this._progress = progress;

        // This variable stores the timestamp of the last time we updated the
        // progress variable when the video is currently playing.
        // This is so that we can always accurately compute the progress we've had,
        // even if the timeout does not exactly work.
        this._lastTime = 0;

        // This variable holds the ID of the timer that updates our progress variable.
        this._timeout = 0;

        // Whether this is a static image get from 'get_current_image'.
        if (!(this._animated = animate))
            return;

        // This time, we don't have to add a timer here, because media streams start
        // paused. However, media streams need to tell GTK once they are initialized,
        // so we do that here.
        // TODO: Report 'prepared' name collision.
        //this.prepared(false, true, true, DURATION);
    }

    vfunc_get_current_image() {
        return this._animated ? new this.constructor({}, false, this._progress) : this;
    }

    vfunc_get_flags() {
        return this._animated ? Gdk.PaintableFlags.SIZE :
            Gdk.PaintableFlags.CONTENTS | Gdk.PaintableFlags.SIZE;
    }

    // This function will be called when a playing stream gets paused.
    vfunc_pause() {
        if (!this._animated)
            return;

        // We remove the updating source here and set it back to 0 so
        // that the finalize function doesn't try to remove it again.
        GLib.source_remove(this._timeout);
        this._timeout = 0;
        this._lastTime = 0;
    }

    vfunc_play() {
        if (!this._animated)
            return false;

        // XXX: The loop property does not persist.
        this.loop = true;

        // If we're already at the end of the stream, we don't want to start playing
        // and exit early.
        if (this._progress >= DURATION)
            return false;

        // This time, we add the source only when we start playing.
        this._timeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 10, () => {
            // Compute the time that has elapsed since the last time we were called
            // and add it to our current progress.
            let currentTime = GLib.main_current_source().get_time();
            this._progress += currentTime - this._lastTime;
            // Update the last time to the current timestamp.
            this._lastTime = currentTime;

            // Check if we've ended.
            if (this._progress > DURATION)
                this._progress = this.loop ? this._progress % DURATION : DURATION;

            // Update the timestamp of the media stream.
            this.update(this._progress);
            // We also need to invalidate our contents again. After all, we are a
            // video and not just an audio stream.
            this.invalidate_contents();

            // Now check if we have finished playing and if so, tell the media stream.
            // The media stream will then call our pause function to pause the stream.
            // TODO: Report 'ended' name collision.
            //if (this._progress >= DURATION)
            //    this.ended();

            // The timeout function is removed by the pause function, so we can just
            // always return this value.
            return GLib.SOURCE_CONTINUE;
        });

        // We also want to initialize our time, so that we can do accurate updates.
        this._lastTime = GLib.get_monotonic_time();

        // We successfully started playing, so we return TRUE here.
        return true;
    }

    // This is optional functionality for media streams, but not being able to seek is
    // kinda boring. And it's trivial to implement, so let's go for it.
    vfunc_seek(timestamp) {
        if (!this._animated) {
            this.seek_failed();
            return;
        }

        this._progress = timestamp;

        // Media streams are asynchronous, so seeking can take a while. We however don't
        // need that functionality, so we can just report success.
        this.seek_success();

        // We also have to update our timestamp and tell the paintable interface about
        // the seek.
        this.update(this._progress);
        this.invalidate_contents();
    }

    // The same function as the "Animated Paintable" demo one.
    vfunc_snapshot(snapshot, width, height) {
        snapshot.append_color(
            new Gdk.RGBA({ red: 0.9, green: 0.75, blue: 0.15, alpha: 1.0 }),
            new Graphene.Rect({ size: { width, height } })
        );

        let min = Math.min(width, height);
        let cr = snapshot.append_cairo(
            new Graphene.Rect().init((width - min) / 2, (height - min) / 2, min, min)
        );

        cr.translate(width / 2, height / 2);
        cr.scale(min, min);
        cr.rotate(2 * Math.PI * this._progress / DURATION);

        cr.arc(0, 0, 0.1, -Math.PI, Math.PI);
        cr.fill();

        cr.setLineWidth(RADIUS);
        cr.setDash([RADIUS * Math.PI / 3], 0);
        cr.arc(0, 0, RADIUS, -Math.PI, Math.PI);
        cr.stroke();

        cr.$dispose();
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application, title: "Nuclear MediaStream",
        defaultWidth: 300, defaultHeight: 200,
        child: new Gtk.Video({
            mediaStream: new NuclearMediaStream({ loop: true }),
        }),
    }).present();
});

application.run([]);
