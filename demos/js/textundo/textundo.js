// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Undo and Redo
 *
 * The GtkTextView supports undo and redo through the use of a
 * GtkTextBuffer. You can enable or disable undo support using
 * gtk_text_buffer_set_enable_undo().
 *
 * Use Control+z to undo and Control+Shift+z or Control+y to
 * redo previously undone operations.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gtk } = imports.gi;

const CANNOT_UNDO =
`The GtkTextView supports undo and redo through the use of a GtkTextBuffer. You can enable or disable undo support using gtk_text_buffer_set_enable_undo().
Type to add more text.
Use Control+z to undo and Control+Shift+z or Control+y to redo previously undone operations.
`;

const CAN_UNDO =
`The insertion above cannot be undone.
`;

let buffer = new Gtk.TextBuffer({ enableUndo: true });

// This text cannot be undone.
buffer.begin_irreversible_action();
buffer.insert(buffer.get_start_iter(), CANNOT_UNDO, -1);
buffer.end_irreversible_action();

// This text can be undone.
buffer.insert(buffer.get_end_iter(), CAN_UNDO, -1);

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application, title: "Undo and Redo",
        resizable: false,
        defaultWidth: 330, defaultHeight: 330,
        child: new Gtk.ScrolledWindow({
            child: new Gtk.TextView({
                wrapMode: Gtk.WrapMode.WORD,
                topMargin: 20, bottomMargin: 20,
                leftMargin: 20, rightMargin: 20,
                pixelsBelowLines: 10,
                buffer,
            }),
        }),
    }).present();
});

application.run([]);
