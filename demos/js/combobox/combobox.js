// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Combo Boxes
 *
 * The GtkComboBox widget allows to select one option out of a list.
 * The GtkComboBoxEntry additionally allows the user to enter a value
 * that is not in the list of options.
 *
 * How the options are displayed is controlled by cell renderers.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GObject, Gtk, Pango } = imports.gi;
const ByteArray = imports.byteArray;
const _ = imports.gettext.domain('gtk40').gettext;

const Column = { ICON_NAME: 0, TEXT: 1 };

const ICONS = [
    { name: 'dialog-warning', label: _("Warning") },
    { name: 'process-stop', label: _("Stop") },
    { name: 'document-new', label: _("New") },
    { name: 'edit-clear', label: _("Clear") },
    { label: "separator" },
    { name: 'document-open', label: _("Open") },
];

const CAPITAL_GROUPS = JSON.parse(ByteArray.toString(
    Gio.File.new_for_path('.').get_child('capitals.json').load_contents(null)[1]
)).groups;

const MASK_PARTS = ['[0-9]*', 'One', 'Two', '2½', 'Three'];

// A simple validating entry.
const MaskEntry = GObject.registerClass({
    GTypeName: 'MaskEntry',
    Properties: {
        'mask': GObject.ParamSpec.string(
            'mask', 'Mask', 'Mask', GObject.ParamFlags.READWRITE, null
        ),
    },
}, class extends Gtk.Entry {
    get mask() {
        return this._regexp && this._regexp.source || '';
    }

    set mask(mask) {
        if (mask != this.mask) {
            this._regexp = new RegExp(mask);
            this.notify('mask');
        }
    }

    vfunc_changed() {
        let attrList = new Pango.AttrList();

        if (this._regexp && !this._regexp[Symbol.match](this.text))
            attrList.insert(Pango.attr_foreground_new(65535, 32767, 32767));

        this.set_attributes(attrList);
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 2,
        marginStart: 10, marginEnd: 10,
        marginTop: 10, marginBottom: 10,
    });

    // A combobox demonstrating cell renderers, separators and insensitive rows.
    {
        let store = Gtk.ListStore.new([GObject.TYPE_STRING, GObject.TYPE_STRING]);

        ICONS.forEach(icon => {
            store.set(store.append(), [Column.ICON_NAME, Column.TEXT], [icon.name || '', icon.label]);
        });

        let combo = new Gtk.ComboBox({ model: store });

        // A GtkCellLayoutDataFunc that demonstrates how one can control sensitivity of rows.
        // This particular function does nothing useful and just makes the second row insensitive.
        let setSensitive = (cellLayout_, cell, treeModel, iter) => {
            cell.sensitive = treeModel.get_path(iter).get_indices()[0] != 1;
        };

        let renderer = new Gtk.CellRendererPixbuf();
        combo.pack_start(renderer, false);
        combo.add_attribute(renderer, 'icon-name', Column.ICON_NAME);
        combo.set_cell_data_func(renderer, setSensitive);

        renderer = new Gtk.CellRendererText();
        combo.pack_start(renderer, true);
        combo.add_attribute(renderer, 'text', Column.TEXT);
        combo.set_cell_data_func(renderer, setSensitive);

        // A GtkTreeViewRowSeparatorFunc that demonstrates how rows can be rendered as separators.
        // This particular function does nothing useful and just turns the fourth row into a separator.
        let isSeparator = (model, iter) => model.get_path(iter).get_indices()[0] == 4;
        combo.set_row_separator_func(isSeparator);

        let box = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginStart: 5, marginEnd: 5,
            marginTop: 5, marginBottom: 5,
        });
        box.append(combo);

        vbox.append(new Gtk.Frame({ label: "Items with icons", child: box }));
    }

    // A combobox demonstrating trees.
    {
        let store = Gtk.TreeStore.new([GObject.TYPE_STRING]);

        CAPITAL_GROUPS.forEach(group => {
            let iter = store.append(null);
            store.set_value(iter, 0, group.name);
            group.capitals.forEach(capital => {
                store.set_value(store.append(iter), 0, capital);
            });
        });

        let combo = new Gtk.ComboBox({ model: store });

        let isCapitalSensitive = (cellLayout_, cell, treeModel, iter) => {
            cell.sensitive = !treeModel.iter_has_child(iter);
        };

        let renderer = new Gtk.CellRendererText();
        combo.pack_start(renderer, true);
        combo.add_attribute(renderer, 'text', 0);
        combo.set_cell_data_func(renderer, isCapitalSensitive);

        let box = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginStart: 5, marginEnd: 5,
            marginTop: 5, marginBottom: 5,
        });
        box.append(combo);

        vbox.append(new Gtk.Frame({ label: "Where are we ?", child: box }));
    }

    // A GtkComboBoxEntry with validation.
    {
        let combo = Gtk.ComboBoxText.new_with_entry();
        MASK_PARTS.slice(1).forEach(maskPart => combo.append_text(maskPart));

        let maskEntry = new MaskEntry({ mask: `^(${MASK_PARTS.join('|')})$` });
        combo.set_child(maskEntry);

        let box = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginStart: 5, marginEnd: 5,
            marginTop: 5, marginBottom: 5,
        });
        box.append(combo);

        vbox.append(new Gtk.Frame({ label: "Editable", child: box }));
    }

    // A combobox with string IDs.
    {
        let combo = new Gtk.ComboBoxText();
        combo.append('never', "Not visible");
        combo.append('when-active', "Visible when active");
        combo.append('always', "Always visible");

        let entry = new Gtk.Entry();
        entry.bind_property('text', combo, 'active-id', GObject.BindingFlags.BIDIRECTIONAL);

        let box = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginStart: 5, marginEnd: 5,
            marginTop: 5, marginBottom: 5,
        });
        box.append(combo);
        box.append(entry);

        vbox.append(new Gtk.Frame({ label: "String IDs", child: box }));
    }

    new Gtk.Window({
        application,
        title: "Combo Boxes",
        child: vbox,
    }).present();
});

application.run([]);
