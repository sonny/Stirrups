// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

const { GObject, Graphene, Gsk, Gtk } = imports.gi;
const { perspective3D } = imports.four_point_transform;

Number.prototype.toRadians = function() {
    return this * Math.PI / 180;
};
Number.prototype.mapOffset = function() {
    let result = this % 180;
    if (result < 0)
        result += 180;

    return result;
};

// Spherical coordinates.
function sx(r, t, p) {
    return r * Math.sin(t) * Math.cos(p);
}
function sy(r, t, p_) {
    return r * Math.cos(t);
}
function sz(r, t, p) {
    return r * Math.sin(t) * Math.sin(p);
}

GObject.registerClass({
    GTypeName: 'Demo2Layout',
    Properties: {
        'offset': GObject.ParamSpec.double(
            'offset', "Offset", "The vertical offset",
            GObject.ParamFlags.READWRITE, Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER, 0
        ),
        'position': GObject.ParamSpec.double(
            'position', "Position", "The horizontal position",
            GObject.ParamFlags.READWRITE, Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER, 0
        ),
    },
}, class extends Gtk.LayoutManager {
    vfunc_get_request_mode() {
        return Gtk.SizeRequestMode.CONSTANT_SIZE;
    }

    vfunc_measure(widget, orientation, forSize_) {
        let [minimumSize, naturalSize] = [0, 0];

        [...widget].forEach(child => {
            if (!child.should_layout())
                return;

            let [childMin, childNat] = child.measure(orientation, -1);
            minimumSize = Math.max(minimumSize, childMin);
            naturalSize = Math.max(naturalSize, childNat);
        });

        let minimum = minimumSize;
        let natural = 3 * naturalSize;
        return [minimum, natural, -1, -1];
    }

    vfunc_allocate(widget, width, height, baseline_) {
        let [childWidth, childHeight] = [0, 0];
        let children = [...widget];

        // For simplicity, assume all children are the same size.
        let childMinReq = widget.get_first_child().get_preferred_size()[0];
        let w = Math.max(childWidth, childMinReq.width);
        let h = Math.max(childHeight, childMinReq.height);

        let [r, x0, y0] = [300, 300, 300];

        children.forEach((child, index) => {
            let j = index / 36;
            let k = index % 36;

            child.set_child_visible(false);

            let p1 = new Graphene.Point3D().init(w, h, 1);
            let p2 = new Graphene.Point3D().init(w, 0, 1);
            let p3 = new Graphene.Point3D().init(0, 0, 1);
            let p4 = new Graphene.Point3D().init(0, h, 1);

            let t_1 = (this.offset + 10 * j).mapOffset().toRadians();
            let t_2 = (this.offset + 10 * (j + 1)).mapOffset().toRadians();
            let p_1 = (this.position + 10 * k).toRadians();
            let p_2 = (this.position + 10 * (k + 1)).toRadians();

            if (t_2 < t_1)
                return;

            if (sz(r, t_1, p_1) > 0 ||
                sz(r, t_2, p_1) > 0 ||
                sz(r, t_1, p_2) > 0 ||
                sz(r, t_2, p_2) > 0)
                return;

            child.set_child_visible(true);

            let q1 = new Graphene.Point3D().init(x0 + sx(r, t_1, p_1), y0 + sy(r, t_1, p_1), sz(r, t_1, p_1));
            let q2 = new Graphene.Point3D().init(x0 + sx(r, t_2, p_1), y0 + sy(r, t_2, p_1), sz(r, t_2, p_1));
            let q3 = new Graphene.Point3D().init(x0 + sx(r, t_2, p_2), y0 + sy(r, t_2, p_2), sz(r, t_2, p_2));
            let q4 = new Graphene.Point3D().init(x0 + sx(r, t_1, p_2), y0 + sy(r, t_1, p_2), sz(r, t_1, p_2));

            // Get a matrix that moves p1 -> q1, p2 -> q2, ...
            let m = perspective3D(p1, p2, p3, p4, q1, q2, q3, q4);

            let transform = new Gsk.Transform().matrix(m);

            // Since our matrix was built for transforming points with z = 1,
            // prepend a translation to the z = 1 plane.
            transform = transform.translate_3d(new Graphene.Point3D().init(0, 0, 1));

            child.allocate(w, h, -1, transform);
        });
    }

    vfunc_notify(pspec) {
        if (['offset', 'position'].includes(pspec.get_name()))
            this.layout_changed();
    }
});
