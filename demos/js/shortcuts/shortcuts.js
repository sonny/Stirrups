// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Shortcuts Window
 *
 * GtkShortcutsWindow is a window that provides a help overlay
 * for shortcuts and gestures in an application.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GObject, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

const HANDLER_NAMES = [
    'shortcuts_builder_shortcuts', 'shortcuts_gedit_shortcuts',
    'shortcuts_clocks_shortcuts', 'shortcuts_clocks_shortcuts_stopwatch',
    'shortcuts_boxes_shortcuts', 'shortcuts_boxes_shortcuts_wizard', 'shortcuts_boxes_shortcuts_display',
];

// Contrary to GObject.registerClass, GObject.Class takes a prototype.
// The proxy returns the requested callbacks to bind the builder handlers to.
const Window = new GObject.Class(new Proxy({
    Name: `DemoWindow`,
    GTypeName: `DemoWindow`,
    Extends: Gtk.Window,

    showAppShortcuts: function(appName, viewName) {
        let shortcutsBuilder = Gtk.Builder.new_from_file(directory.get_child(`shortcuts-${appName}.ui`).get_path());
        let shortcutsWindow = shortcutsBuilder.get_object(`shortcuts-${appName}`);
        shortcutsWindow.set_transient_for(this);
        shortcutsWindow.viewName = viewName || null;
        shortcutsWindow.show();
    }
}, {
    // Declare the callbacks.
    ownKeys: function(target) {
        return Reflect.ownKeys(target).concat(HANDLER_NAMES);
    },

    // Provide the callbacks.
    getOwnPropertyDescriptor: function(target, prop) {
        if (HANDLER_NAMES.includes(prop)) {
            let [, appName,, viewName] = prop.split('_');

            // 'this' is a TemplatedWindow instance.
            let value = function() {
                this.showAppShortcuts(appName, viewName);
            };

            return { configurable: true, enumerable: true, value };
        }

        return Reflect.getOwnPropertyDescriptor(target, prop);
    }
}));

// For obscure reasons, templating does not work with GObject.Class.
const TemplatedWindow = GObject.registerClass({
    GTypeName: 'TemplatedDemoWindow',
    Template: directory.get_child('shortcuts.ui').get_uri(),
}, class extends Window {});

let application = new Gtk.Application();

application.connect('activate', () => {
    new TemplatedWindow({ application }).present();
});

application.run([]);
