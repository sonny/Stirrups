// SPDX-FileCopyrightText: 2019 Red Hat, Inc.
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-FileContributor: Author: Matthias Clasen <mclasen@redhat.com>
// SPDX-License-Identifier: GPL-3.0-or-later

/* exported Tag, TaggedEntry */

const { Gdk, GObject, Gtk } = imports.gi;

GObject.registerClassWithAdditionalDescriptors = function(metaInfo, klass) {
    Object.defineProperties(klass.prototype, metaInfo.Descriptors);
    delete metaInfo.Descriptors;

    return GObject.registerClass(metaInfo, klass);
};

var Tag = GObject.registerClass({
    CssName: 'tag', // <=> Tag.set_css_name('tag');
    Properties: {
        'has-close-button': GObject.ParamSpec.boolean(
            'has-close-button', "Has close button", "Whether this tag has a close button",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.EXPLICIT_NOTIFY, false
        ),
        'label': GObject.ParamSpec.string(
            'label', "Label", "The tag label",
            GObject.ParamFlags.READWRITE, ''
        ),
    },
    Signals: {
        'button-clicked': {},
        'clicked': {},
    },
}, class DemoTag extends Gtk.Widget {
    vfunc_constructed() {
        let box = new Gtk.Box();
        box.append(new Gtk.Label());
        box.set_parent(this);

        let gesture = new Gtk.GestureClick();
        gesture.connect('released', () => {
            this.emit('clicked');
        });
        this.add_controller(gesture);

        super.vfunc_constructed();
    }

    vfunc_measure(orientation, forSize) {
        return this.get_first_child().measure(orientation, forSize);
    }

    // No dispose/finalize vfuncs in JS.
    vfunc_unrealize() {
        super.vfunc_unrealize();

        [...this].forEach(child => child.unparent());
    }

    vfunc_size_allocate(width, height, baseline) {
        this.get_first_child().size_allocate(new Gdk.Rectangle({ width, height }), baseline || -1);
    }

    get hasCloseButton() {
        return this.get_first_child().get_last_child() instanceof Gtk.Button;
    }

    set hasCloseButton(hasCloseButton) {
        if (this.hasCloseButton == hasCloseButton)
            return;

        if (!hasCloseButton) {
            this.get_first_child().remove(this.get_first_child().get_last_child());
        } else {
            let button = new Gtk.Button({
                iconName: 'window-close-symbolic',
                halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER,
                hasFrame: false,
            });
            button.connect('clicked', () => this.emit('button-clicked'));

            this.get_first_child().append(button);
        }
    }

    get label() {
        return this.get_first_child().get_first_child().label;
    }

    set label(label) {
        if (this.label == label)
            return;

        this.get_first_child().get_first_child().label = label;
        this.notify('label');
    }
});

const editablePropertyNames = [
    'text', 'cursor-position', 'enable-undo', 'selection-bound',
    'editable', 'width-chars', 'max-width-chars', 'xalign',
];

// Gtk.Editable has helper functions to bound toplevel and delegate widget properties.
// However it seems to be unsable from language bindings.
var TaggedEntry = GObject.registerClassWithAdditionalDescriptors({
    Implements: [Gtk.Editable],

    // The Gtk.Editable properties to override.
    Properties: Object.fromEntries(
        editablePropertyNames.map(
            propertyName => [propertyName, GObject.ParamSpec.override(propertyName, Gtk.Editable)]
        )
    ),

    // The descriptors for the Gtk.Editable properties.
    Descriptors: Object.fromEntries(
        editablePropertyNames.map(
            propertyName => [propertyName, {
                enumerable: true,
                get() {
                    return this._delegate[propertyName];
                },
                set(value) {
                    this._delegate[propertyName] = value;
                }
            }]
        )
    ),
}, class DemoTaggedEntry extends Gtk.Widget {
    _init(params) {
        super._init({
            cssClasses: ['tagged'],
        });

        this._delegate = new Gtk.Text({
            hexpand: true, vexpand: true,
            widthChars: 6, maxWidthChars: 6,
        });
        this._delegate.set_parent(this);
        this.init_delegate();
    }

    vfunc_get_delegate() {
        return this._delegate;
    }

    vfunc_grab_focus() {
        return this._delegate.grab_focus();
    }

    // No dispose/finalize vfuncs in JS.
    vfunc_unrealize() {
        super.vfunc_unrealize();

        this.finish_delegate();
        delete this._delegate;
        [...this].forEach(child => this.remove(child));
    }

    addChild(widget) {
        widget.set_parent(this);
    }

    insertChildAfter(widget, previousSibling) {
        widget.insert_after(this, previousSibling);
    }

    remove(child) {
        child.unparent();
    }
});
TaggedEntry.set_accessible_role(Gtk.AccessibleRole.TEXT_BOX);
TaggedEntry.set_layout_manager_type(Gtk.BoxLayout.$gtype);
TaggedEntry.set_css_name('entry');
