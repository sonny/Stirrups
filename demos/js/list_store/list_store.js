// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* List Store
 *
 * The GtkListStore is used to store data in list form, to be used
 * later on by a GtkTreeView to display it. This demo builds a
 * simple GtkListStore and displays it.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, GObject, Gtk, Pango } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');

// A convenient class for a tree view column with a unique renderer.
const OneRendererColumn = GObject.registerClass({
    Properties: {
        'attributes': GObject.ParamSpec.jsobject(
            'attributes', "Attributes", "The attribute map for the renderer",
            GObject.ParamFlags.READWRITE,
        ),
        'renderer': GObject.ParamSpec.object(
            'renderer', "Renderer", "The unique renderer",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT, Gtk.CellRenderer.$gtype
        ),
    },
}, class extends Gtk.TreeViewColumn {
    get attributes() {
        return this._attributes ?? null;
    }

    set attributes(attributes) {
        if (this.attributes == attributes)
            return;

        if (attributes && !this.renderer)
            throw new Error('Setting attributing without renderer');

        if (attributes)
            for (let attribute in attributes)
                this.add_attribute(this.renderer, attribute, attributes[attribute]);
        else if (this.renderer)
            this.clear_attributes(this.renderer);


        this._attributes = attributes;
        this.notify('attributes');
    }

    get renderer() {
        return this._renderer ?? null;
    }

    set renderer(renderer) {
        if (this.renderer == renderer)
            return;

        if (renderer)
            this.pack_start(renderer, false);
        else
            this.clear();

        this._renderer = renderer;
        this.notify('renderer');
    }
});

const bugs = JSON.parse(
    ByteArray.toString(
        directory.get_child('bugs.json').load_contents(null)[1]
    )
);

const Column = {
    FIXED: 0,
    PROJECT: 1,
    NUMBER: 2,
    DESCRIPTION: 3,
    PULSE: 4,
    ICON: 5,
    ACTIVE: 6,
    SENSITIVE: 7,
};

let store = Gtk.ListStore.new([
    GObject.TYPE_BOOLEAN,
    GObject.TYPE_STRING,
    GObject.TYPE_UINT,
    GObject.TYPE_STRING,
    GObject.TYPE_UINT,
    GObject.TYPE_STRING,
    GObject.TYPE_BOOLEAN,
    GObject.TYPE_BOOLEAN,
]);

// Add data to the list store.
bugs.forEach((bug, index) => {
    let iconName = index == 1 || index == 3 ? 'battery-caution-charging-symbolic' : '';
    let sensitive = index != 3;

    store.set(store.append(), Object.values(Column), [
        bug.fixed,
        bug.project,
        bug.number,
        bug.description,
        0,
        iconName,
        false,
        sensitive,
    ]);
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let treeView = new Gtk.TreeView({
        model: store,
        searchColumn: Column.DESCRIPTION,
        tooltipColumn: Column.DESCRIPTION,
        vexpand: true,
    });

    // Column for fixed toggles.
    treeView.append_column(new OneRendererColumn({
        title: "Fixed?",
        // Set this column to a fixed sizing (of 50 pixels).
        // TODO: Report issue to GTK: Fixed size is too small.
        // "Gtk-CRITICAL: Allocation width too small. Tried to allocate 50x25, but GtkButton needs at least 56x25".
        sizing: Gtk.TreeViewColumnSizing.FIXED, fixedWidth: 56,
        renderer: new Gtk.CellRendererToggle(),
        attributes: {
            'active': Column.FIXED,
        },
    }));
    treeView.get_column(0).renderer.connect('toggled', (renderer_, pathString) => {
        let iter = treeView.model.get_iter_from_string(pathString)[1];
        let fixed = treeView.model.get_value(iter, Column.FIXED);
        treeView.model.set_value(iter, Column.FIXED, !fixed);
    });

    // Column for bug projects.
    treeView.append_column(new OneRendererColumn({
        title: "Project",
        sortColumnId: Column.PROJECT,
        renderer: new Gtk.CellRendererText(),
        attributes: {
            'text': Column.PROJECT,
        },
    }));

    // Column for bug numbers.
    treeView.append_column(new OneRendererColumn({
        title: "Bug number",
        // TODO: Report issue to GTK: Column sorting is not working.
        sortColumnId: Column.NUMBER,
        renderer: new Gtk.CellRendererText(),
        attributes: {
            'text': Column.NUMBER,
        },
    }));

    // Column for description.
    treeView.append_column(new OneRendererColumn({
        title: "Description",
        sortColumnId: Column.DESCRIPTION,
        renderer: new Gtk.CellRendererText({
            widthChars: 60, ellipsize: Pango.EllipsizeMode.END,
        }),
        attributes: {
            'text': Column.DESCRIPTION,
        },
    }));

    // Column for spinner.
    treeView.append_column(new OneRendererColumn({
        title: "Spinning",
        sortColumnId: Column.PULSE,
        renderer: new Gtk.CellRendererSpinner(),
        attributes: {
            'pulse': Column.PULSE,
            'active': Column.ACTIVE
        },
    }));

    // Column for symbolic icon.
    treeView.append_column(new OneRendererColumn({
        title: "Symblic icon",
        sortColumnId: Column.ICON,
        renderer: new Gtk.CellRendererPixbuf(),
        attributes: {
            'icon-name': Column.ICON,
            'sensitive': Column.SENSITIVE
        },
    }));

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        marginTop: 8, marginBottom: 8,
        marginStart: 8, marginEnd: 8,
        spacing: 8,
    });
    vbox.append(new Gtk.Label({
        label: "This is a list of bugs encountered in JS demos.",
    }));
    vbox.append(new Gtk.ScrolledWindow({
        hasFrame: true,
        hscrollbarPolicy: Gtk.PolicyType.NEVER,
        child: treeView,
    }));

    new Gtk.Window({
        application, title: "List Store",
        defaultWidth: 280, defaultHeight: 250,
        child: vbox,
    }).present();

    GLib.timeout_add(GLib.PRIORITY_DEFAULT, 80, () => {
        let iter = treeView.model.get_iter_first()[1];
        let pulse = treeView.model.get_value(iter, Column.PULSE);
        treeView.model.set(
            iter,
            [Column.PULSE, Column.ACTIVE],
            [pulse == GLib.MAXUINT32 ? 0 : pulse + 1, true]
        );

        return GLib.SOURCE_CONTINUE;
    });
});

application.run([]);
