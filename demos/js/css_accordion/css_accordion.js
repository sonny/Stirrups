// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* CSS Accordion
 *
 * A simple accordion demo written using CSS transitions and multiple backgrounds
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

const TEXT = "This Is A CSS ACCORDION :-)";

let application = new Gtk.Application();

application.connect('activate', () => {
    let container = new Gtk.Box({
        halign: Gtk.Align.CENTER,
        valign: Gtk.Align.CENTER,
    });

    TEXT.split(' ').forEach(label => {
        container.append(new Gtk.Button({ label }));
    });

    let window = new Gtk.Window({
        application,
        title: "CSS Accordion",
        defaultWidth: 600,
        defaultHeight: 300,
        child: container
    });

    let provider = new Gtk.CssProvider();
    provider.load_from_file(directory.get_child('css_accordion.css'));
    Gtk.StyleContext.add_provider_for_display(window.display, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    window.present();
});

application.run([]);
