// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Stack Sidebar
 *
 * GtkStackSidebar provides an automatic sidebar widget to control
 * navigation of a GtkStack object. This widget automatically updates
 * its content based on what is presently available in the GtkStack
 * object, and using the "title" child property to set the display labels.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;

const PAGES = Object.assign([
    "Welcome to GTK",
    "GtkStackSidebar Widget",
    "Automatic navigation",
    "Consistent appearance",
    "Scrolling",
    "Page 6", "Page 7", "Page 8", "Page 9",
], {
    [Symbol.iterator]: function* () {
        yield new Gtk.Image({
            cssClasses: ['icon-dropshadow'],
            gicon: new Gio.ThemedIcon({ names: ['org.codeberg.som.Stirrups', 'org.gtk.Demo4'] }),
            pixelSize: 256,
            name: this[0],
        });

        for (let i = 1; i < this.length; i++)
            yield new Gtk.Label({ label: this[i], name: this[i] });
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let stack = new Gtk.Stack({
        transitionType: Gtk.StackTransitionType.SLIDE_UP_DOWN,
        hexpand: true,
    });

    for (let widget of PAGES)
        stack.add_named(widget, widget.name).title = widget.name;

    let box = new Gtk.Box();
    box.append(new Gtk.StackSidebar({ stack }));
    box.append(stack);

    let window = new Gtk.Window({
        application,
        title: "Stack Sidebar",
        resizable: true,
        child: box,
    });
    window.set_titlebar(new Gtk.HeaderBar());
    window.present();
});

application.run([]);
