// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Sliding Puzzle
 *
 * This demo demonstrates how to use gestures and paintables to create a
 * small sliding puzzle game.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, GObject, Gtk } = imports.gi;
const { NuclearAnimation } = imports.nuclearanimation;
const { PuzzlePiece } = imports.puzzlepiece;
const imageFile = Gio.File.new_for_path('.').get_child('portland-rose.jpg');
const videoFile = Gio.File.new_for_path('.').get_child('gtk-logo.webm');

const MIN_SIZE = 2;
const MAX_SIZE = 10;
const DEFAULT_SIZE = 3;

const Puzzle = GObject.registerClass({
    Properties: {
        'paintable': GObject.ParamSpec.object(
            'paintable', "Paintable", "The paintable the puzzle is based on",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, Gdk.Paintable.$gtype
        ),
        'solved': GObject.ParamSpec.boolean(
            'solved', "Solved", "Whether the puzzle is solved",
            GObject.ParamFlags.READWRITE, false
        ),
        'width': GObject.ParamSpec.uint(
            'width', "Width", "The puzzle width, in cell unit",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, MIN_SIZE, MAX_SIZE, DEFAULT_SIZE
        ),
        'height': GObject.ParamSpec.uint(
            'height', "Height", "The puzzle height, in cell unit",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, MIN_SIZE, MAX_SIZE, DEFAULT_SIZE
        ),
    },
}, class Puzzle extends Gtk.Grid {
    static Movement = {
        LEFT: {
            keyvals: [Gdk.KEY_Left, Gdk.KEY_KP_Left],
            dx: -1, dy: 0,
        },
        RIGHT: {
            keyvals: [Gdk.KEY_Right, Gdk.KEY_KP_Right],
            dx: 1, dy: 0,
        },
        UP: {
            keyvals: [Gdk.KEY_Up, Gdk.KEY_KP_Up],
            dx: 0, dy: -1,
        },
        DOWN: {
            keyvals: [Gdk.KEY_Down, Gdk.KEY_KP_Down],
            dx: 0, dy: 1,
        },
    };

    _init(params) {
        super._init(Object.assign({
            focusable: true,
            // Make sure the cells have equal size.
            rowHomogeneous: true,
            columnHomogeneous: true,
        }, params));

        // Use the arrow keys to move the pieces.
        let shortcutController = new Gtk.ShortcutController({ scope: Gtk.ShortcutScope.LOCAL });
        Object.values(Puzzle.Movement).forEach(movement => {
            shortcutController.add_shortcut(new Gtk.Shortcut({
                trigger: new Gtk.AlternativeTrigger({
                    first: new Gtk.KeyvalTrigger({ keyval: movement.keyvals[0] }),
                    second: new Gtk.KeyvalTrigger({ keyval: movement.keyvals[1] }),
                }),
                action: Gtk.CallbackAction.new(this._onKeyPressed.bind(this, movement)),
            }));
        });
        this.add_controller(shortcutController);

        // Use the button click to move the pieces.
        let gesture = new Gtk.GestureClick();
        gesture.connect('pressed', this._onButtonPressed.bind(this));
        this.add_controller(gesture);

        this.start();
    }

    _checkSolved() {
        // Nothing to check if the puzzle is already solved.
        if (this.solved)
            return true;

        // If the empty cell isn't in the bottom right, the puzzle is obviously not solved.
        if (this._emptyX != this.width - 1 || this._emptyY != this.height - 1)
            return false;

        // Check that all pieces are in the right position.
        for (let y = 0; y < this.height; y++)
            for (let x = 0; x < this.width; x++) {
                let piece = this.get_child_at(x, y).paintable;

                // Ignore the empty cell.
                if (!piece)
                    continue;

                if (piece.hPosition != x || piece.vPosition != y)
                    return false;
            }

        // We solved the puzzle.
        this.solved = true;

        // Fill the empty cell to show that we're done.
        this.get_child_at(this._emptyX, this._emptyY).paintable = new PuzzlePiece({
            hPosition: this._emptyX, vPosition: this._emptyY,
            hTotal: this.width, vTotal: this.height,
            sourcePaintable: this.paintable,
        });

        return true;
    }

    _move(movement) {
        // We don't move anything if the puzzle is solved.
        if (this.solved)
            return false;

        let { dx, dy } = movement;

        // Check if we can move the piece.
        if (
            (dx < 0 && this._emptyX < -dx) ||
            (dx + this._emptyX >= this.width) ||
            (dy < 0 && this._emptyY < -dy) ||
            (dy + this._emptyY >= this.height)
        )
            return false;

        // Compute the new empty position.
        let nextEmptyX = this._emptyX + dx;
        let nextEmptyY = this._emptyY + dy;

        // Get the current and next empty picture.
        let emptyChild = this.get_child_at(this._emptyX, this._emptyY);
        let nextEmptyChild = this.get_child_at(nextEmptyX, nextEmptyY);

        // Move the displayed piece.
        emptyChild.paintable = nextEmptyChild.paintable;
        nextEmptyChild.paintable = null;

        // Update the current empty position.
        this._emptyX = nextEmptyX;
        this._emptyY = nextEmptyY;

        // We successfully moved the piece.
        return true;
    }

    _onButtonPressed(gesture_, nPress_, x, y) {
        let child = this.pick(x, y, Gtk.PickFlags.DEFAULT);
        if (!child) {
            this.error_bell();
            return;
        }

        let [column, row] = this.query_child(child);
        if (column == this._emptyX && row != this._emptyY) {
            let pos = this._emptyY;
            for (let i = row; i < pos; i++)
                if (!this._move(Puzzle.Movement.UP))
                    this.error_bell();
            for (let i = pos; i < row; i++)
                if (!this._move(Puzzle.Movement.DOWN))
                    this.error_bell();
        } else if (column != this._emptyX && row == this._emptyY) {
            let pos = this._emptyX;
            for (let i = column; i < pos; i++)
                if (!this._move(Puzzle.Movement.LEFT))
                    this.error_bell();
            for (let i = pos; i < column; i++)
                if (!this._move(Puzzle.Movement.RIGHT))
                    this.error_bell();
        } else {
            this.error_bell();
        }

        this._checkSolved();
    }

    _onKeyPressed(movement) {
        if (this._move(movement))
            this._checkSolved();
        else
            this.error_bell();

        // Handle the key, even though we didn't do anything to the puzzle.
        return Gdk.EVENT_STOP;
    }

    // Inform all the pieces that we don't need them anymore.
    destroy() {
        [...this].forEach(picture => picture.paintable?.destroy());
    }

    shuffle() {
        let movements = Object.values(Puzzle.Movement);

        // Do many random moves.
        for (let i = 0; i < this.width * this.height * 50; i++) {
            let randomIndex = Math.floor(Math.random() * movements.length);
            this._move(movements[randomIndex]);
        }
    }

    start() {
        this._emptyX = this.width - 1;
        this._emptyY = this.height - 1;

        // Add a picture for every cell.
        for (let vPosition = 0; vPosition < this.height; vPosition++)
            for (let hPosition = 0; hPosition < this.width; hPosition++) {
                let piece = hPosition == this._emptyX && vPosition == this._emptyY ? null : new PuzzlePiece({
                    hPosition, vPosition,
                    hTotal: this.width, vTotal: this.height,
                    sourcePaintable: this.paintable,
                });

                // Inform the old piece, if exists, that we don't need it anymore.
                this.get_child_at(hPosition, vPosition)?.paintable?.destroy();

                this.attach(new Gtk.Picture({
                    keepAspectRatio: false,
                    paintable: piece,
                }), hPosition, vPosition, 1, 1);
            }

        this.shuffle();
        this.solved = false;
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    // The puzzle container, whose aspect ratio can be manually adjusted.
    let puzzleFrame = new Gtk.AspectFrame({ obeyChild: false });

    // The 3 puzzle source paintables.
    let choices = new Gtk.FlowBox({
        cssClasses: ['view'],
    });
    [
        Gdk.Texture.new_from_file(imageFile),
        new NuclearAnimation(),
        Object.assign(
            Gtk.MediaFile.new_for_file(videoFile),
            { loop: true, muted: true, playing: true }
        )
    ].forEach(paintable => {
        choices.insert(new Gtk.Image({ iconSize: Gtk.IconSize.LARGE, paintable }), -1);
    });

    // How many pieces the puzzle will be clipped in.
    let sizeSpin = Gtk.SpinButton.new_with_range(MIN_SIZE, MAX_SIZE, 1);
    sizeSpin.set_value(DEFAULT_SIZE);

    let applyButton = new Gtk.Button({
        label: "Apply",
        halign: Gtk.Align.END,
    });
    applyButton.connect('clicked', () => {
        applyButton.get_ancestor(Gtk.Popover).popdown();

        puzzleFrame.child?.destroy();

        puzzleFrame.child = new Puzzle({
            // Take the first choice if none is selected (startup).
            paintable: (choices.get_selected_children()[0] ?? choices.get_first_child()).child.paintable,
            width: sizeSpin.get_value_as_int(),
            height: sizeSpin.get_value_as_int(),
        });
        puzzleFrame.ratio = puzzleFrame.child.paintable.get_intrinsic_aspect_ratio() || 1;
        puzzleFrame.child.grab_focus();
    });

    let restartButton = new Gtk.Button({
        iconName: 'view-refresh-symbolic',
    });
    restartButton.connect('clicked', () => {
        if (puzzleFrame.child.solved)
            puzzleFrame.child.start();
        else
            puzzleFrame.child.shuffle();

        puzzleFrame.child.grab_focus();
    });

    let popoverGrid = new Gtk.Grid({
        marginStart: 10, marginEnd: 10,
        marginTop: 10, marginBottom: 10,
        rowSpacing: 10, columnSpacing: 10,
    });
    popoverGrid.attach(new Gtk.ScrolledWindow({ child: choices }), 0, 0, 2, 1);
    popoverGrid.attach(new Gtk.Label({ label: "Size", xalign: 0 }), 0, 1, 1, 1);
    popoverGrid.attach(sizeSpin, 1, 1, 1, 1);
    popoverGrid.attach(applyButton, 1, 2, 1, 1);

    let tweakButton = new Gtk.MenuButton({
        iconName: 'emblem-system-symbolic',
        popover: new Gtk.Popover({ child: popoverGrid }),
    });

    let headerBar = new Gtk.HeaderBar();
    headerBar.pack_start(restartButton);
    headerBar.pack_end(tweakButton);

    let window = new Gtk.Window({
        application, title: "Sliding Puzzle",
        defaultWidth: 400, defaultHeight: 300,
        child: puzzleFrame,
    });

    window.set_titlebar(headerBar);
    window.present();

    // Init the puzzle.
    applyButton.emit('clicked');
});

application.run([]);
