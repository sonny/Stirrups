// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Font Rendering
 *
 * Demonstrates various aspects of font rendering.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, GdkPixbuf, Gio, Gtk, Pango, PangoCairo } = imports.gi;
const Cairo = imports.cairo;
const directory = Gio.File.new_for_path('.');

class FontsRenderer {
    constructor(builder) {
        this._builder = builder;
        this._pangoContext = builder.get_object('image').create_pango_context();
        this._scale = 9;

        builder.get_object('down_button').connect('clicked', this._scaleDown.bind(this));
        builder.get_object('up_button').connect('clicked', this._scaleUp.bind(this));
        builder.get_object('entry').connect('notify::text', this.updateImage.bind(this));
        builder.get_object('font_button').connect('notify::font-desc', this.updateImage.bind(this));
        builder.get_object('hinting').connect('notify::active', this.updateImage.bind(this));
        builder.get_object('hint_metrics').connect('notify::active', this.updateImage.bind(this));
        builder.get_object('show_extents').connect('notify::active', this.updateImage.bind(this));
        builder.get_object('show_grid').connect('notify::active', this.updateImage.bind(this));
        builder.get_object('text_radio').connect('notify::active', this.updateImage.bind(this));
    }

    _scaleDown() {
        this._scale -= 1;
        this._updateButtons();
        this.updateImage();
    }

    _scaleUp() {
        this._scale += 1;
        this._updateButtons();
        this.updateImage();
    }

    _updateButtons() {
        this._builder.get_object('down_button').sensitive = this._scale > 1;
        this._builder.get_object('up_button').sensitive = this._scale < 32;
    }

    updateImage() {
        let layout = Pango.Layout.new(this._pangoContext);
        layout.set_font_description(this._builder.get_object('font_button').get_font_desc());
        let pixbuf;

        if (this._builder.get_object('text_radio').active) {
            layout.set_text(this._builder.get_object('entry').text, -1);
            let [ink, logical] = layout.get_extents();
            let baseline = layout.get_baseline();
            let pixelInk = new Pango.Rectangle(ink);
            Pango.extents_to_pixels(pixelInk, null);

            let surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, pixelInk.width + 20, pixelInk.height + 20);
            let cr = new Cairo.Context(surface);
            cr.setSourceRGB(1, 1, 1);
            cr.paint();
            cr.setSourceRGB(0, 0, 0);
            cr.moveTo(10, 10);
            PangoCairo.show_layout(cr, layout);
            cr.$dispose();

            pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, surface.getWidth(), surface.getHeight());
            pixbuf = pixbuf.scale_simple(pixbuf.width * this._scale, pixbuf.height * this._scale, GdkPixbuf.InterpType.NEAREST);
            surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, pixbuf.width, pixbuf.height);
            cr = new Cairo.Context(surface);
            Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0);
            cr.paint();

            if (this._builder.get_object('show_grid').active) {
                cr.setLineWidth(1);
                cr.setSourceRGBA(0, 0, 0.2, 0.2);

                for (let i = 1; i < pixelInk.height + 20; i++) {
                    cr.moveTo(0, this._scale * i - 0.5);
                    cr.lineTo(this._scale * (pixelInk.width + 20), this._scale * i - 0.5);
                    cr.stroke();
                }

                for (let i = 1; i < pixelInk.width + 20; i++) {
                    cr.moveTo(this._scale * i - 0.5, 0);
                    cr.lineTo(this._scale * i - 0.5, this._scale * (pixelInk.height + 20));
                    cr.stroke();
                }
            }

            if (this._builder.get_object('show_extents').active) {
                cr.setLineWidth(1);
                cr.setSourceRGB(1, 0, 0);

                cr.rectangle(
                    this._scale * (10 + Pango.units_to_double(logical.x)) - 0.5,
                    this._scale * (10 + Pango.units_to_double(logical.y)) - 0.5,
                    this._scale * Pango.units_to_double(logical.width) + 1,
                    this._scale * Pango.units_to_double(logical.height) + 1
                );
                cr.stroke();
                cr.moveTo(
                    this._scale * (10 + Pango.units_to_double(logical.x)) - 0.5,
                    this._scale * (10 + Pango.units_to_double(baseline)) - 0.5
                );
                cr.lineTo(
                    this._scale * (10 + Pango.units_to_double(logical.x + logical.width)) + 1,
                    this._scale * (10 + Pango.units_to_double(baseline)) - 0.5
                );
                cr.stroke();

                cr.setSourceRGB(0, 0, 1);

                cr.rectangle(
                    this._scale * (10 + Pango.units_to_double(ink.x)) + 0.5,
                    this._scale * (10 + Pango.units_to_double(ink.y)) + 0.5,
                    this._scale * Pango.units_to_double(ink.width) - 1,
                    this._scale * Pango.units_to_double(ink.height) - 1
                );
                cr.stroke();
            }

            cr.$dispose();
            pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, surface.getWidth(), surface.getHeight());
        } else {
            // XXX: Cannot access glyph info array from GJS.
            // So use text spaces instead of modifying glyph geometry.
            layout.set_text('a a a a ', -1);
            let pixelLogical = layout.get_pixel_extents()[1];

            let surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, pixelLogical.width, 4 * pixelLogical.height);
            let cr = new Cairo.Context(surface);
            cr.setSourceRGB(1, 1, 1);
            cr.paint();

            cr.setSourceRGB(0, 0, 0);
            for (let j = 0; j < 4; j++) {
                cr.moveTo(0, j * pixelLogical.height);
                PangoCairo.show_layout(cr, layout);
            }

            cr.$dispose();
            pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, surface.getWidth(), surface.getHeight());
            pixbuf = pixbuf.scale_simple(pixbuf.width * this._scale, pixbuf.height * this._scale, GdkPixbuf.InterpType.NEAREST);
        }

        this._builder.get_object('image').set_pixbuf(pixbuf);
    }
}

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('fontrendering.ui').get_path());

    // XXX: No Cairo font options in GJS implementation.
    let hinting = builder.get_object('hinting');
    hinting.append_text('Default');
    hinting.active = hinting.model.iter_n_children(null) - 1;
    hinting.sensitive = false;
    builder.get_object('hint_metrics').sensitive = false;

    new FontsRenderer(builder).updateImage();

    let window = builder.get_object('window');
    window.set_application(application);
    window.present();
});

application.run([]);
