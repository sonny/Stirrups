// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Transparency
 *
 * Blur the background behind an overlay.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gio, Gtk } = imports.gi;
const { BlurOverlay } = imports.bluroverlay;
const directory = Gio.File.new_for_path('.');

let application = new Gtk.Application();

application.connect('activate', () => {
    let blurOverlay = new BlurOverlay({
        child: new Gtk.Picture({
            file: directory.get_child('portland-rose.jpg'),
        }),
    });

    blurOverlay.addOverlay(new Gtk.Button({
        child: new Gtk.Label({
            label: "Don't click this button!",
            marginStart: 50, marginEnd: 50,
            marginTop: 50, marginBottom: 50,
        }),
        opacity: 0.7,
        valign: Gtk.Align.START,
    }), 5);

    blurOverlay.addOverlay(new Gtk.Button({
        child: new Gtk.Label({
            label: "Maybe this one?",
            marginStart: 50, marginEnd: 50,
            marginTop: 50, marginBottom: 50,
        }),
        opacity: 0.7,
        valign: Gtk.Align.END,
    }), 5);

    new Gtk.Window({
        application, title: "Transparency",
        defaultWidth: 450, defaultHeight: 450,
        child: blurOverlay,
        visible: true,
    });
});

application.run([]);
