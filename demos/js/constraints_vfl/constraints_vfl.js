// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* VFL
 *
 * GtkConstraintLayout allows defining constraints using a
 * compact syntax called Visual Format Language, or VFL.
 *
 * A typical example of a VFL specification looks like this:
 *
 * H:|-[button1(==button2)]-12-[button2]-|
 */

imports.gi.versions['Gtk'] = '4.0';
const { GObject, Gtk } = imports.gi;

/* Layout:
 *
 *   +-----------------------------+
 *   | +-----------+ +-----------+ |
 *   | |  Child 1  | |  Child 2  | |
 *   | +-----------+ +-----------+ |
 *   | +-------------------------+ |
 *   | |         Child 3         | |
 *   | +-------------------------+ |
 *   +-----------------------------+
 *
 * Constraints:
 *
 *   super.start = child1.start - 8
 *   child1.width = child2.width
 *   child1.end = child2.start - 12
 *   child2.end = super.end - 8
 *   super.start = child3.start - 8
 *   child3.end = super.end - 8
 *   super.top = child1.top - 8
 *   super.top = child2.top - 8
 *   child1.bottom = child3.top - 12
 *   child2.bottom = child3.top - 12
 *   child3.height = child1.height
 *   child3.height = child2.height
 *   child3.bottom = super.bottom - 8
 *
 * Visual format:
 *
 *   H:|-8-[view1(==view2)-12-[view2]-8-|
 *   H:|-8-[view3]-8-|
 *   V:|-8-[view1]-12-[view3(==view1)]-8-|
 *   V:|-8-[view2]-12-[view3(==view2)]-8-|
 */

const VflGrid = GObject.registerClass(class VflGrid extends Gtk.Widget {
    _init(params) {
        super._init(params);

        this._button1 = new Gtk.Button({ label: "Child 1", name: 'button1' });
        this._button2 = new Gtk.Button({ label: "Child 2", name: 'button2' });
        this._button3 = new Gtk.Button({ label: "Child 3", name: 'button3' });

        this._button1.set_parent(this);
        this._button2.set_parent(this);
        this._button3.set_parent(this);

        const vfl = [
            "H:|-[button1(==button2)]-12-[button2]-|",
            "H:|-[button3]-|",
            "V:|-[button1]-12-[button3(==button1)]-|",
            "V:|-[button2]-12-[button3(==button2)]-|",
        ];

        const [hspacing, vspacing] = [8, 8];

        const nameMap = {
            'button1': this._button1,
            'button2': this._button2,
            'button3': this._button3,
        };

        try {
            this.layoutManager.add_constraints_from_description(vfl, hspacing, vspacing, nameMap);
        } catch(e) {
            log(`VFL parsing error:\n${e.message}`);
        }
    }

    // Cannot use vfunc_dispose() in GJS because it conflicts with the garbage collector.
    vfunc_unrealize() {
        super.vfunc_unrealize();

        this._button1.unparent();
        this._button2.unparent();
        this._button3.unparent();
    }
});
VflGrid.set_layout_manager_type(Gtk.ConstraintLayout.$gtype);

let application = new Gtk.Application();

application.connect('activate', () => {
    let grid = new VflGrid({
        hexpand: true, vexpand: true,
    });

    let box = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 12,
    });
    box.append(grid);

    new Gtk.Window({
        application,
        title: "Constraints — VFL",
        defaultWidth: 260,
        child: box,
    }).present();
});

application.run([]);
