/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const GLib = imports.gi.GLib;
const ByteArray = imports.byteArray;
const System = imports.system;

const SEPARATOR = /^[-=~]*$/;

let path = ARGV[0] || null;

if (!path || !GLib.file_test(path, GLib.FileTest.EXISTS)) {
    print('');
    System.exit(1);
}

let contents = ByteArray.toString(GLib.file_get_contents(path)[1]);
let lines = contents.split('\n')
    .map(line => line.trim())
    .filter(line => line && !line.startsWith('#'));

let version, date;
let isInList = false;
let xml = '';

lines.forEach((line, index) => {
    if (lines[index + 1]?.match(SEPARATOR)) {
        if (isInList) {
            isInList = false;
            xml += `</ul>`;
        }
        if (version) {
            xml += `</description>`;
            xml += `</release>`;
        }
        [version, date] = line.split(' - ');
        xml += `<release version="${version}" date="${date}">`;
        xml += `<description>`;
    } else if (line.match(SEPARATOR)) {
        return;
    } else if (isInList) {
        if (line.startsWith('*')) {
            xml += `<li>${line.replace('*', '').trim()}</li>`;
        } else {
            isInList = false;
            xml += `</ul>`;
            xml += `<p>${line}</p>`;
        }
    } else {
        if (line.startsWith('*')) {
            isInList = true;
            xml += `<ul>`;
            xml += `<li>${line.replace('*', '').trim()}</li>`;
        } else {
            xml += `<p>${line}</p>`;
        }
    }
});

if (isInList)
    xml += `</ul>`;

if (version) {
    xml += `</description>`;
    xml += `</release>`;
}

print(xml);
